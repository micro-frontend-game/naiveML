"use strict";

function appendASTChild(parent, child) {
  if (!parent.children) parent.children = []
  parent.children.push(child)
}

function toCamelCase(kebabStr) {
  return kebabStr.replace(/-([a-z])/g, (_, l)=> l.toUpperCase())
}

export class NaiveMLCompiller {

  /**
   * Convert naiveML to an useful data structure.
   * @param {string} naiveML - the code representing the DOM.
   * @param {Object} options - configure the builder behavior.
   * @param {string} options.componentId - set a "data-<componentId>" attribute to each element.
   * @param {function} options.loader - set the lazy loader to external components.
   */
  constructor(naiveML='', options={}) {
    this.__componentId = options.componentId
    this.__loader = options.loader
    this.parse(naiveML)
  }

  /**
   * Build a compreensive structure representing the naiveML code tokens.
   */
  parse(naiveML) {
    const codeLines = naiveML
    .split(/\r*\n\r*/)
    .map(line => line.match(/^(\s*)(.*)/))
    .map(line => [ line[1].length, line[2] ])
    this.__AST = this.__parseTrunk(codeLines, 1)
    return this.__AST
  }

  __parseTrunk(codeLines, startLine) {
    const firstValidLine = codeLines.findIndex(line => !!line[1])
    if (firstValidLine === -1) return null
    let root = this.__tokenizeLine(
      codeLines[firstValidLine][1],
      startLine + firstValidLine
    )
    let childIndent = null
    let newChildBranch = null
    let newChildBranchIndex = null
    const buildCurrentChild = ()=>
      this.__parseTrunk(newChildBranch, startLine+newChildBranchIndex)
    codeLines.forEach((line, index) => {
      if (index <= firstValidLine) return
      if (!line[1]) return
      if (!childIndent) childIndent = line[0]
      if (line[0] < childIndent) {
        throw Error(`Bad indentation at line ${startLine+index}. Expected ${childIndent}, found ${line[0]}.`)
      }
      if (childIndent === line[0]) {
        // found a new child branch
        if (newChildBranch) {
          appendASTChild(root, buildCurrentChild())
        }
        newChildBranchIndex = index
        newChildBranch = []
      }
      newChildBranch.push(line)
    });
    // Add the last children:
    if (newChildBranch) {
      appendASTChild(root, buildCurrentChild())
    }
    return root
  }

  __tokenizeLine(line, lineNum) {
    if (line.match(/^\s/)) throw Error(`Line tokenizer expect a unindented string. At${lineNum}: <${line}>`)
    const match = line.match(/^([^\s]+)(\s+(.*))?$/)
    if (!match) throw Error(`Fail to tokenize line ${lineNum}: <${line}>`)
    let [, nodeName, , attrsStr] = match
    const node = { node: nodeName }
    if (nodeName === '@text') {
      node.text = attrsStr
    } else {
      Object.assign(node, this.__tokenizeAttrs(nodeName, attrsStr, lineNum))
    }
    return node
  }

  __tokenizeAttrs(nodeName, attrsStr, lineNum) {
    let attrs, specialAttrs
    try {
      attrsStr = (attrsStr||'')
                 .replace(/^\s*([^\s"]*)="/g, '"$1":"')
                 .replace(/"\s*([^\s"]*)="/g, '", "$1":"')
      attrs = JSON.parse('{' + attrsStr + '}')
    } catch(err) {
      throw Error(`Invalid attributes at line ${lineNum}: <${attrsStr}>`)
    }
    [, nodeName, specialAttrs ] = nodeName.match(/^([^.#]*(?:\(.*\))?)([.#][^)]+)?$/)
    ;(specialAttrs||'')
    .replace(/([.#])/g, ' $1')
    .trim().split(' ')
    .filter(attr => !!attr)
    .forEach(attr => {
      attr = attr.replace(/^(.)/, '$1 ').split(' ')
      if (attr[0] === '#') {
        attrs.id = attr[1]
      } else {
        if (!attrs.class) attrs.class = ''
        attrs.class = `${attrs.class} ${attr[1]}`.trim()
      }
    })
    if (nodeName.match(/^@load\(/)) {
      const match = nodeName.match(/^@load\(([^,)]+)(?:,([^)]+))?\)$/)
      if (!match) throw Error(`Invalid @load definition at line ${lineNum}: <${nodeName}>`)
      nodeName = '@load'
      attrs['@'] = { url: match[1] }
      specialAttrs = (match[2]||'')
      .split(',').filter(att => !!att).map(att => {
        att = att.split(':')
        attrs['@'][toCamelCase(att[0])] = att[1]
      })
    }
    return { ...attrs, node: nodeName }
  }

  /**
   * Build a DOM tree this instance data.
   */
  buildDOM() {
    return this.__astToDOM(this.__AST)
  }

  __astToDOM(ast) {
    if (ast.node == '@text') {
      return document.createTextNode(ast.text)
    }
    let el
    if (ast.node == '@load') {
      el = document.createElement(ast['@'].loadingTag || 'div')
      this.__setElAttributes(el, {
        ...ast,
        class: `${ast.class||''} ${ast['@'].loadingClass || 'loading'}`.trim()
      })
      this.__loader(ast['@'].url)
      .then(dynEl => {
        this.__setElAttributes(dynEl, ast)
        this.__addElChildren(dynEl, ast)
        el.before(dynEl)
        el.remove()
      })
      .catch(err => {
        // TODO: Makes error element configurable.
        console.error(`Fail to load "${ast['@'].url}".`, err)
        el.appendChild(document.createTextNode(`
        Fail to load "${ast['@'].url}". ${err.message}
        `))
      })
    } else {
      el = document.createElement(ast.node)
      this.__setElAttributes(el, ast)
      this.__addElChildren(el, ast)
    }
    return el
  }

  __setElAttributes(el, ast) {
    Object.entries(ast)
    .filter(([attr,])=> !~['node','text','@','children'].indexOf(attr))
    .forEach(([attr, value])=> el.setAttribute(attr, value))
    if (this.__componentId) {
      el.dataset[toCamelCase(this.__componentId)] = ''
    }
  }

  __addElChildren(el, ast) {
    if (ast.text) {
      el.appendChild(document.createTextNode(ast.text))
    }
    ;(ast.children||[])
    .forEach(child => el.appendChild(this.__astToDOM(child)))
  }

}

/**
 * Build a DOM tree from the provided naiveML code.
 * @param {string} naiveML - the code representing the DOM.
 * @param {Object} options - configure the builder behavior.
 * @param {string} options.componentId - set a "data-<componentId>" attribute to each element.
 * @param {function} options.loader - set the lazy loader to external components.
 */
export function build(naiveML, options) {
    const compiler = new NaiveMLCompiller(naiveML, options)
    return compiler.buildDOM()
}
