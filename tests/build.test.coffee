getBodyHTML = ()=>
  document.body.innerHTML
  .replace /</g, '\n<'
  .replace />/g, '>\n'
  .replace /\n+/gsm, '\n'
  .trim()
  .replace /([ \t]+)(\n|$)/, (_,s,n)-> ('🔳' for i in [1..s.length]).join('') + n

describe 'NaiveML Builder', ->

  beforeEach ->
    buildVirtualDOM()

  it 'builds a simple tag', ->
    build('div').should.be.instanceof(window.HTMLDivElement)

  it 'can be atached to the document', ->
    document.body.appendChild build '''
      div#box align="right"
        p text="A paragraph.  "
        @text A free text node.
        span
          img.pic src="/path/to/pic.jpg"
    '''
    getBodyHTML().should.be.equal '''
      <div align="right" id="box">
      <p>
      A paragraph.🔳🔳
      </p>
      A free text node.
      <span>
      <img src="/path/to/pic.jpg" class="pic">
      </span>
      </div>
    '''

  it 'can be atached to the document with custom component id', ->
    document.body.appendChild build '''
      div#box align="right"
        p text="A paragraph.  "
        @text A free text node.
        span
          img.pic src="/path/to/pic.jpg"
    ''', componentId: 'my-test'
    getBodyHTML().should.be.equal '''
      <div align="right" id="box" data-my-test="">
      <p data-my-test="">
      A paragraph.🔳🔳
      </p>
      A free text node.
      <span data-my-test="">
      <img src="/path/to/pic.jpg" class="pic" data-my-test="">
      </span>
      </div>
    '''

  it 'lazy loads a component', ->
    document.body.appendChild build '''
      div#box text="Some text."
        @load(/path/to/mod.js)#great att1="val1"
    ''', componentId: 'my-test', loader: (modName)->
      Promise.timeout(100).then ->
        document.createElement modName.replace /[^a-z]/g, ''
    getBodyHTML().should.be.equal '''
      <div id="box" data-my-test="">
      Some text.
      <div att1="val1" id="great" class="loading" data-my-test="">
      </div>
      </div>
    '''
    Promise.timeout(200).then ->
      getBodyHTML().should.be.equal '''
        <div id="box" data-my-test="">
        Some text.
        <pathtomodjs att1="val1" id="great" data-my-test="">
        </pathtomodjs>
        </div>
      '''

  it 'lazy loads a component with children', ->
    document.body.appendChild build '''
      @load(/path/to/mod.js)#great text="Some text"
        strong text="Bold!"
    ''', componentId: 'my-test', loader: (modName)->
      Promise.timeout(100).then ->
        document.createElement modName.replace /[^a-z]/g, ''
    getBodyHTML().should.be.equal '''
      <div id="great" class="loading" data-my-test="">
      </div>
    '''
    Promise.timeout(200).then ->
      getBodyHTML().should.be.equal '''
        <pathtomodjs id="great" data-my-test="">
        Some text
        <strong data-my-test="">
        Bold!
        </strong>
        </pathtomodjs>
      '''
