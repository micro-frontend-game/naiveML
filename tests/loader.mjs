import chalk from 'chalk'
import jsdom from 'jsdom'

global.JSDOM = jsdom.JSDOM

global.buildVirtualDOM = ()=> {
  const virtualConsole = new jsdom.VirtualConsole()
  virtualConsole.sendTo(console)
  const dom = new JSDOM(
    '<!DOCTYPE html><html><body></body></html>',
    { virtualConsole }
  )
  global.window = dom.window
  global.document = dom.window.document
}

let reloadingTimes = 0

export const mochaHooks = {

  beforeAll() {
    reloadingTimes++
    const reloadStr = reloadingTimes === 1 ? 'Loading' : 'Reloading'
    process.stdout.write(chalk.gray(reloadStr+' NaiveML module...'))
    return import('../naiveML.mjs?cache='+Date.now()).then(module => {
      process.stdout.write(chalk.blue(' Done.\n'))
      global.naiveML = module
      global.build = module.build
    })
  }

}

Promise.timeout = ms => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

Promise.prototype.timeout = function(ms) {
  return this.then(arg => {
    return new Promise(resolve => {
      setTimeout(()=> resolve(arg), ms)
    })
  })
}
