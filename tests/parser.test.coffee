describe 'NaiveML Parser', ->

  compiler = null
  beforeEach ->
    compiler = new naiveML.NaiveMLCompiller()

  it 'tokenizes a line with no attributes', ->
    compiler.__tokenizeLine('div')
    .should.be.deep.equal {
      node: 'div'
    }

  it 'tokenizes a line with some attributes', ->
    compiler.__tokenizeLine('div id="the-box" align="right" data-x="123"')
    .should.be.deep.equal {
      node: 'div', id: 'the-box', align: 'right', 'data-x': '123'
    }

  it 'tokenizes a line with simple id and class definition', ->
    compiler.__tokenizeLine('div#the-box.alert')
    .should.be.deep.equal {
      node: 'div', id: 'the-box', class: 'alert'
    }

  it 'tokenizes a line with simple id and double class definition', ->
    compiler.__tokenizeLine('div#the-box.alert.big')
    .should.be.deep.equal {
      node: 'div', id: 'the-box', class: 'alert big'
    }

  it 'tokenizes a line with simple class and id (inverse) definition', ->
    compiler.__tokenizeLine('div#.alert#the-box')
    .should.be.deep.equal {
      node: 'div', id: 'the-box', class: 'alert'
    }

  it 'tokenizes a line with mixed simple id and double class definition', ->
    compiler.__tokenizeLine('div.alert#the-box.big')
    .should.be.deep.equal {
      node: 'div', id: 'the-box', class: 'alert big'
    }

  it 'tokenizes a text node line', ->
    compiler.__tokenizeLine('@text This is a sample text.')
    .should.be.deep.equal {
      node: '@text', 'text': 'This is a sample text.'
    }

  it 'tokenizes a lazy load component', ->
    compiler.__tokenizeLine('@load(url/to/some/component.js).some-class att="val"')
    .should.be.deep.equal {
      node: '@load', class: 'some-class', att: 'val', '@': {
        url: 'url/to/some/component.js'
      }
    }

  it 'tokenizes a lazy load component with customized loading class', ->
    compiler.__tokenizeLine('@load(url/to/some/component.js,loading-class:wait).some-class att="val"')
    .should.be.deep.equal {
      node: '@load', class: 'some-class', att: 'val', '@': {
        url: 'url/to/some/component.js',
        loadingClass: 'wait'
      }
    }

  it 'tokenizes with a lazy load component with customized loading class and loading tag', ->
    compiler.__tokenizeLine('@load(url/to/some/component.js,loading-class:wait,loading-tag:b)')
    .should.be.deep.equal {
      node: '@load', '@': {
        url: 'url/to/some/component.js',
        loadingClass: 'wait',
        loadingTag: 'b'
      }
    }

  it 'recognizes identation', ->
    compiler.parse("""
    div
      ul
        li text="111"
        li text="222"
      p
          b text="bold"
      hr
      p
        i text="italic"
    """).should.be.deep.equal {
      node: 'div'
      children: [
        {
          node: 'ul'
          children: [
            { node: 'li', text: '111' }
            { node: 'li', text: '222' }
          ]
        }
        {
          node: 'p'
          children: [
            { node: 'b', text: 'bold' }
          ]
        }
        { node: 'hr' }
        {
          node: 'p'
          children: [
            { node: 'i', text: 'italic' }
          ]
        }
      ]
    }

  it 'notices bad indentation', ->
    (-> compiler.parse """
    ul
        li text="111"
      li text="222"
    """).should.throw 'Bad indentation at line 3. Expected 4, found 2.'

  it 'ignores blank lines', ->
    compiler.parse("""

    ul

      li text="111"


      li text="222"

    """).should.be.deep.equal {
      node: 'ul'
      children: [
        { node: 'li', text: '111' }
        { node: 'li', text: '222' }
      ]
    }

  it 'ignores blank lines and notices bad indentation', ->
    (-> compiler.parse """

    ul

        li text="111"


      li text="222"

    """).should.throw 'Bad indentation at line 7. Expected 4, found 2.'
