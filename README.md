NaiveML
=======

A simple code that can be easily translated to HTML, using indentation as nested description and lazy loading for this main project components.

You can see it working at https://micro-frontend-game.gitlab.io

```js
import { buildNaiveML } from '.../naiveML.js'
import { myPictureURL } from '.../pics.js'

document.body.appendChild(
    buildNaiveML(`
    div#my-root
        img.my-pic src="${myPictureURL}"
        @text This is a simple text in a anonymous element.
        pre text="This is a pre formated text.\nWith two lines."
        @load(url/to/some/component.js).with-a-class param1="value1" param2="value2"
            p text="A paragraph."
            hr
            p text="Another paragraph."
        @load(url/to/some/component.js).yes-again param1="other value"
            strong text="Some bold words!"
        @load(url/to/other/component.js) data-key="data-value"
    `,
    { componentId: 'my-component' })
)
```

Developing
----------

* Clone it:
  ```bash
  git clone git@gitlab.com:micro-frontend-game/naiveML.git # or from your remote clone
  cd naiveML
  ```
* Install dev dependencies:
  ```bash
  npm install
  ```
* Edit some files;
* Write some tests;
* Test it:
  ```bash
  npm test
  ```
* Push and open a merge request.

If you want to auto run tests after changing any file, run:
```bash
npm run test:watch
```
